
# As Input for our algorithm, we need a capacity matrix, a cost matrix, and the position of the source and sink vertex
Capacities = [
    [0, 2, 4, 0, 0],
    [0, 0, 0, 5, 0],
    [0, 0, 0, 6, 0],
    [0, 0, 0, 0, 5],
    [0, 0, 0, 0, 0],
]
Cost = [
    [0, 3, 1, 0, 0],
    [-3, 0, 0, 4, 0],
    [-1, 0, 0, 1, 0],
    [0, -4, -1, 0, 1],
    [0, 0, 0, -1, 0],
]

source = 0
sink = 4

# The function max_flow_min_value returns the resulting flow graph F (matrix) in the end
# We first use Ford-Fulkerson to find the maximum flow and then the Cycle Canceling Algorithm to minimize its costs
def max_flow_min_value(Cap, s, t):
    n = len(Cap)
    F = [[0] * n for x in range(n)]
    path = bfs(Cap, s, t, F)
    while path is not None:
        aug_value = min([Cap[i][j] - F[i][j] for (i, j) in path])
        for (i, j) in path:
            F[i][j] += aug_value
            F[j][i] -= aug_value
        path = bfs(Cap, s, t, F)
    # max_flow_value = sum(F[0])
    min_mean_value = min(find_negative_cycle_value(Cost, Cap, F, 0), find_negative_cycle_value(Cost, Cap, F, n - 1))
    if min_mean_value >= 0:
        return F
    else:
        path = find_negative_cycle_path(Cost, Cap, F, min_mean_value)
        while min_mean_value < 0:
            aug_value = min([Cap[i][j] - F[i][j] for (i, j) in path])
            for (i, j) in path:
                F[i][j] += aug_value
                F[j][i] -= aug_value
            min_mean_value = min(find_negative_cycle_value(Cost, Cap, F, 0), find_negative_cycle_value(Cost, Cap, F, n - 1))
            path = find_negative_cycle_path(Cost, Cap, F, min_mean_value)
        return F


def bfs(Cap, s, t, F):
    queue = [s]
    flow = {s: []}
    if s == t:
        return flow
    while len(queue) > 0:
        u = queue.pop(0)
        for v in range(len(Cap)):
            if Cap[u][v] - F[u][v] > 0 and v not in flow:
                flow[v] = flow[u] + [(u, v)]
                queue.append(v)
                if v == t:
                    return flow[t]


# We use the Bellman Ford algorithm to detect negative cycles
def find_negative_cycle_path(Cost, Cap, F, min_mean_value):
    n = len(Cost)
    for start in [0, n - 1]:
        Dist = [[9999, -1] for x in range(n)]
        Dist[start][0] = 0
        Dist[start][1] = -1
        last_updated = None
        for x in range(n):
            for i in range(n):
                for j in range(n):
                    if Cap[i][j] - F[i][j] > 0 and Dist[i][0] + Cost[i][j] - min_mean_value <= Dist[j][0]:
                        Dist[j][0] = Dist[i][0] + Cost[i][j] - min_mean_value
                        Dist[j][1] = i
                        if x == n - 1:
                            last_updated = j
        seen = []
        neg_cyc_flow = []
        if last_updated is not None:  # If a negative cycle exists, we find it as follows:
            seen.append(Dist[last_updated][1])
            for j in range(n):
                seen.append(Dist[seen[-1]][1])
                if seen.count(j) == 2:
                    seen = []
                    seen.append(j)
                    while seen.count(j) == 1:
                        seen.append(Dist[seen[-1]][1])
                    seen1 = seen[:0:-1]
                    seen2 = seen[-2::-1]
                    y = zip(seen1, seen2)
                    neg_cyc_flow = tuple(y)
                    return neg_cyc_flow


def find_negative_cycle_value(Cost, Cap, F, start):
    n = len(Cost)
    dp = [[9999 for x in range(n + 1)] for x in range(n)]
    dp[start][0] = 0
    for k in range(1, n + 1):
        for i in range(n):
            for j in range(n):
                if Cap[j][i] - F[j][i] > 0:
                    dp[i][k] = min(dp[i][k], (dp[j][k-1] + Cost[j][i]))
    min_mean_value = min([
        max(
            [(dp[j][n] - dp[j][k]) / (n - k) for k in range(n)]
        )
        for j in range(n)])
    return min_mean_value


print(max_flow_min_value(Capacities, source, sink))

